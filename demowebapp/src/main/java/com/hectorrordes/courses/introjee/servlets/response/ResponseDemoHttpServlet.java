package com.hectorrordes.courses.introjee.servlets.response;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ResponseDemoHttpServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2085088758195062841L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String redirectType = request.getParameter("redirectType");
		
		if(redirectType == null || redirectType.equals("clientRedirect")) {
			response.sendRedirect("clientRedirect.jsp");
			return;
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("/forwardServlet");
			//request.setAttribute(", arg1)
			
			rd.forward(request, response);
		}
		
		
	}

}

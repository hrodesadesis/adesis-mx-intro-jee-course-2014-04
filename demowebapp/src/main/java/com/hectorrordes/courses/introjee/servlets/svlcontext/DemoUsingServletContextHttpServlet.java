package com.hectorrordes.courses.introjee.servlets.svlcontext;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DemoUsingServletContextHttpServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7588857331262376276L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		TotalRequestsCounter counter = (TotalRequestsCounter) request.getSession().getServletContext().getAttribute(ServletContextAttributes.TOTAL_REQUESTS_ATTR);
		
		int totalRequests = counter.addRequest();
		
		
		PrintWriter pw = response.getWriter();
		
		pw.print("<html><body><h1>Total Requests " + totalRequests+ "</h1></body></html>");
		pw.flush();
		pw.close();
	}
	
	

}

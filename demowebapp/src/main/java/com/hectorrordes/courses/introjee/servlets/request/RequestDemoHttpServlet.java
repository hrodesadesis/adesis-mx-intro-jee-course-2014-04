package com.hectorrordes.courses.introjee.servlets.request;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequestDemoHttpServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758347078653256649L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.getParameter("");
		
		request.getParameterValues("");
		
		request.getHeader("");
		
		request.getHeaders("");
		
		request.getContentLength();
		
		request.getContentType();
	}
	
	
	

}

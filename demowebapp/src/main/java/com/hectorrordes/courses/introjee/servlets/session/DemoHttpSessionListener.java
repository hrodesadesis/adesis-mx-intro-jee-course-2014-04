package com.hectorrordes.courses.introjee.servlets.session;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class DemoHttpSessionListener implements HttpSessionListener {

	public void sessionCreated(HttpSessionEvent sessionCreatedEvent) {
		System.err.println("Session created");

	}

	public void sessionDestroyed(HttpSessionEvent sessionDestroyedEvent) {
		System.err.println("Session destroyed");

	}

}

package com.hectorrordes.courses.introjee.servlets.svlcontext;

import java.io.Serializable;

public class TotalRequestsCounter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5687361786877802292L;

	private int totalRequests;
	
	public synchronized int addRequest(){
		return totalRequests++;
	}
	
	public int getTotalRequests() {
		return totalRequests;
	}
}

package com.hectorrordes.courses.introjee.servlets.helloworld;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns={"/annotationHelloWorld"})
public class HelloWorldAnnotationHttpServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3659971978262144842L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String name = request.getParameter("name");
		
		HelloWorldUtils.sendHelloWorldMessage("Annotations - " + name, response);
	}
}

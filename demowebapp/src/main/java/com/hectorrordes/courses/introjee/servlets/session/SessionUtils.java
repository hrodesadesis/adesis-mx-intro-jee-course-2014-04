package com.hectorrordes.courses.introjee.servlets.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtils {

	public static int increaseRequestsCounter(HttpServletRequest request) {
		// Returns session, if it does not exists it won't be created
		HttpSession session;

		session = request.getSession(false);

		if (session == null) {
			session = request.getSession();
		}

		SessionCounter sessionCounter;
		if (session.getAttribute("counter") == null) {
			session.setAttribute("counter", new SessionCounter());
		}
		sessionCounter = (SessionCounter) session.getAttribute("counter");
		sessionCounter.setTotalRequestsInSession(sessionCounter.getTotalRequestsInSession() + 1);
		
		return sessionCounter.getTotalRequestsInSession();
	}
}

package com.hectorrordes.courses.introjee.servlets.svlcontext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class DemoServletContextListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent event) {

		TotalRequestsCounter counter = (TotalRequestsCounter) event.getServletContext().getAttribute(ServletContextAttributes.TOTAL_REQUESTS_ATTR);
		
		System.out.println("Total Requests "+counter.getTotalRequests());
	}

	public void contextInitialized(ServletContextEvent event) {
		event.getServletContext().setAttribute(ServletContextAttributes.TOTAL_REQUESTS_ATTR, new TotalRequestsCounter());
	}

}

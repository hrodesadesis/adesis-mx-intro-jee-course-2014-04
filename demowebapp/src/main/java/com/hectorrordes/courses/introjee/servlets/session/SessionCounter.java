package com.hectorrordes.courses.introjee.servlets.session;

import java.io.Serializable;

public class SessionCounter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4599641330086864990L;

	private int totalRequestsInSession;

	public int getTotalRequestsInSession() {
		return totalRequestsInSession;
	}

	public void setTotalRequestsInSession(int totalRequestsInSession) {
		this.totalRequestsInSession = totalRequestsInSession;
	}
	
	
}

package com.hectorrordes.courses.introjee.servlets.session;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SessionDemoHttpServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4875330521807268504L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	
		int totalRequests = SessionUtils.increaseRequestsCounter(request);
		
		PrintWriter writer = response.getWriter();
		
		writer.print("<html><body><h1>" + totalRequests +"</html></body></h1>");
		writer.flush();

	}

	

}
